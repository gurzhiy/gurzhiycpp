//
//  minimax.hpp
//  Right in Two
//
//  Created by Vladyslav Gurzhiy on 12/11/15.
//  Copyright © 2015 Gurzhiy. All rights reserved.
//

#ifndef array_hpp
#define array_hpp

void allocateArray(int* &pointerToMainArray, int sizeOfArray);
void releaseArray(int* &pointerToMainArray);
void initArray(int* &pointerToMainArray, int sizeOfArray);
void showArray(int* &pointerToMainArray, int sizeOfArray);
void getArrayMinimum(int* &pointerToMainArray, int sizeOfArray, int &minArrayNumber);
void getArrayMaximum(int* &pointerToMainArray, int sizeOfArray, int &MaxNumberInArray);
void getArrayAverage(int* &pointerToMainArray, int sizeOfArray, int &AvgNumberInArray);
void bubbleSort(int* pointerToMainArray, int sizeOfArray);
void insertValueToBeggining(int* &pointerToMainArray, int &sizeOfArray, int value);
void insertValueToEnd(int* &pointerToMainArray, int &sizeOfArray, int value);

#endif /* array_hpp */
