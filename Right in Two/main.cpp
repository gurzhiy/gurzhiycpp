//
//  main.cpp
//  Right in Two
//
//  Created by Vladyslav Gurzhiy on 12/7/15.
//  Copyright © 2015 Gurzhiy. All rights reserved.
//

#include "sizeof.hpp"
#include "square.hpp"
#include "array.hpp"
#include <iostream>
#include <time.h>
using namespace std;


class Array
{
    
public:
    
    Array()
    {
        m_Size = 0;
        m_Array = nullptr;
        m_Min = 0;
        m_Max = 0;
        m_Avg = 0;
    }
    
    Array(uint32_t size)
    {
        m_Array = nullptr;
        init(size);
    }
    
    ~Array()
    {
        if (m_Array != nullptr)
        {
            delete[] m_Array;
        }
    }
    
    void init(uint32_t size)
    {
        m_Size = size;
        
        if (m_Array != nullptr)
        {
            delete[] m_Array;
        }
        
        m_Array = new int[m_Size];
        
        for (uint32_t i = 0; i < m_Size; i++)
        {
            m_Array[i] = rand()%100;
        }
    }
    
    void print()
    {
        for (int i = 0; i < m_Size; i++)
        {
            std::cout << m_Array[i] << " ";
        }
        
        std::cout << std::endl;
    }
    
    int getMinimum()
    {
        m_Min = m_Array[0];
        for (int i = 1; i < m_Size; i++)
        {
            if (m_Min > m_Array[i])
            {
                m_Min = m_Array[i];
            }
        }
        return m_Min;
    }
    
    int getMaximum()
    {
        m_Max = m_Array[0];
        for (int i = 1; i < m_Size; i++)
        {
            if (m_Max < m_Array[i])
            {
                m_Max = m_Array[i];
            }
        }
        return m_Max;
    }
    
    int getAverage()
    {
        int sum = 0;
        for (int i = 0; i < m_Size; i++)
        {
            sum = sum + m_Array[i];
        }
        m_Avg = sum/m_Size;
        
        return m_Avg;
    }
    
    void insertValueToBeggining(uint32_t value)
    {
        if (m_Array == nullptr)
        {
            return;
        }
        
        
        int* tmpArray = new int[m_Size + 1];
        tmpArray[0] = value;
        tmpArray[m_Size] = m_Array[m_Size  - 1];
        for (int i = 1; i < m_Size ; i++)
        {
            tmpArray[i] = m_Array[i-1];
        }

        delete[] m_Array;
        m_Array = tmpArray;
        m_Size++;
    }
    
    void insertValueToEnd(uint32_t value)
    {
        if (m_Array == nullptr)
        {
            return;
        }
        
        int* tmpArray = new int[m_Size+ 1];
        tmpArray[m_Size] = value;
        for (int i = 0; i < m_Size; i++)
        {
            tmpArray[i] = m_Array[i];
        }
        
        delete[] m_Array;
        m_Array = tmpArray;
        m_Size++;
    }
    
    void bubbleSort()
    {
        int temp;
        for(int i = 1; i < m_Size; i++)
        {
            for(int j = 0; j < (m_Size - i); j++)
                if( m_Array[j] > m_Array[j+1])
                {
                    temp = m_Array[j];
                    m_Array[j] = m_Array[j+1];
                    m_Array[j+1] = temp;
                }
        }
    }
private:
    
    int* m_Array;
    uint32_t m_Size;
    uint32_t m_Min;
    uint32_t m_Max;
    uint32_t m_Avg;
};


int main() {
    srand(time_t(NULL));
    Array arr1;
    arr1.init(10);
    arr1.print();
    arr1.getMinimum();
    cout << "Minimum number of the array is: " << arr1.getMinimum() << endl;
    cout << "Maximums number of the array is: " << arr1.getMaximum() << endl;
    cout << "Average number of the array is: " << arr1.getAverage() << endl;
    arr1.insertValueToBeggining(88);
    arr1.print();
    arr1.insertValueToEnd(22);
    arr1.print();
    arr1.insertValueToEnd(2222);
    arr1.print();
    arr1.bubbleSort();
    arr1.print();
    
    
//    int sizeOfArray = 0;
//    int *pointerToArray = NULL;
//    int MinNumberInArray = 0;
//    int &linkMinNumberInArray = MinNumberInArray;
//    int MaxNumberInArray = 0;
//    int &linkMaxNumberInArray = MaxNumberInArray;
//    int AvgNumberInArray = 0;
//    int &linkAvgNumberInArray = AvgNumberInArray;
//    
//    cout << "Enter wanted number of elements in array, please: ";
//    cin >> sizeOfArray;
//    
//    allocateArray(pointerToArray, sizeOfArray);
//    initArray(pointerToArray, sizeOfArray);
//    showArray(pointerToArray, sizeOfArray);
//    getArrayMinimum(pointerToArray, sizeOfArray, linkMinNumberInArray);
//    getArrayMaximum(pointerToArray, sizeOfArray, linkMaxNumberInArray);
//    getArrayAverage(pointerToArray, sizeOfArray, linkAvgNumberInArray);
//    bubbleSort(pointerToArray, sizeOfArray);
//    cout << "Minimum number of the array is: "<< linkMinNumberInArray <<endl;
//    cout << "Maximum number of the array is: "<< linkMaxNumberInArray <<endl;
//    cout << "Maximum number of the array is: "<< linkAvgNumberInArray <<endl;
//    cout << "Sorted array: " << endl;
//    showArray(pointerToArray, sizeOfArray);
//    insertValueToBeggining(pointerToArray, sizeOfArray, 8);
//    showArray(pointerToArray, sizeOfArray);
//    insertValueToEnd(pointerToArray, sizeOfArray, 12);
//    showArray(pointerToArray, sizeOfArray);
//    bubbleSort(pointerToArray, sizeOfArray);
//    showArray(pointerToArray, sizeOfArray);
//    releaseArray(pointerToArray);

return 0;

}
