//
//  array.cpp
//  Right in Two
//
//  Created by Vladyslav Gurzhiy on 12/11/15.
//  Copyright © 2015 Gurzhiy. All rights reserved.
//

#include "array.hpp"
#include <iostream>
using namespace std;


void allocateArray(int* &pointerToMainArray, int sizeOfArray) {
    //int* pointerToMainArray = &pointerToArray;
    pointerToMainArray = new int[sizeOfArray];
    
}

void releaseArray(int* &pointerToMainArray) {
    if(pointerToMainArray){
        delete [] pointerToMainArray;
        pointerToMainArray = NULL;
    }
}

void initArray(int* &pointerToMainArray, int sizeOfArray) {
    for (int i = 0; i < sizeOfArray; i++) {
        pointerToMainArray[i] = rand()%100;
    }
}

void showArray(int* &pointerToMainArray, int sizeOfArray) {
    for (int i = 0; i < sizeOfArray; i++) {
        cout << pointerToMainArray[i] << "\t";
    }
    cout << "\n\n";
}

void getArrayMinimum(int* &pointerToMainArray, int sizeOfArray, int &minArrayNumber) {
    minArrayNumber = *pointerToMainArray;
    for (int i = 1; i < sizeOfArray; i++) {
        if (minArrayNumber > *(pointerToMainArray + i)) {
            minArrayNumber = *(pointerToMainArray + i);
        }
    }
}

void getArrayMaximum(int* &pointerToMainArray, int sizeOfArray, int &MaxNumberInArray) {
    MaxNumberInArray = *pointerToMainArray;
    for (int i = 1; i < sizeOfArray; i++) {
        if (MaxNumberInArray < *(pointerToMainArray + i)) {
            MaxNumberInArray = *(pointerToMainArray + i);
        }
    }
}

void getArrayAverage(int* &pointerToMainArray, int sizeOfArray, int &AvgNumberInArray) {
    int sum = *pointerToMainArray;
    for (int i = 1; i < sizeOfArray; i++) {
        sum = sum + (*(pointerToMainArray + i));
    }
    AvgNumberInArray = sum/sizeOfArray;
}

void bubbleSort(int* pointerToMainArray, int sizeOfArray)
{
    int temp;
    for(int i = 1; i < sizeOfArray; i++)
    {
        for(int j = 0; j < (sizeOfArray - i); j++)
            if( pointerToMainArray[j] > pointerToMainArray[j+1])
            {
                temp = pointerToMainArray[j];
                pointerToMainArray[j] = pointerToMainArray[j+1];
                pointerToMainArray[j+1] = temp;
            }
    }
}

void insertValueToBeggining(int* &pointerToMainArray, int &sizeOfArray, int value)
{
    if (pointerToMainArray == nullptr)
    {
        return;
    }
    
    
    int* tmpArray = new int[sizeOfArray + 1];
    tmpArray[0] = value;
    tmpArray[sizeOfArray] = pointerToMainArray[sizeOfArray - 1];
    for (int i = 1; i < sizeOfArray; i++)
    {
        tmpArray[i] = pointerToMainArray[i-1];
    }
    
    delete[] pointerToMainArray;
    pointerToMainArray = tmpArray;
    sizeOfArray++;
}

void insertValueToEnd(int* &pointerToMainArray, int &sizeOfArray, int value)
{
    if (pointerToMainArray == nullptr)
    {
        return;
    }
    
    int* tmpArray = new int[sizeOfArray + 1];
    tmpArray[sizeOfArray] = value;
    for (int i = 0; i < sizeOfArray; i++)
    {
        tmpArray[i] = pointerToMainArray[i];
    }
    
    delete[] pointerToMainArray;
    pointerToMainArray = tmpArray;
    sizeOfArray++;
}