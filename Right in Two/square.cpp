//
//  square.cpp
//  Quadratic formula
//
//  Created by Vladyslav Gurzhiy on 12/10/15.
//  Copyright © 2015 Gurzhiy. All rights reserved.
//

#include "square.hpp"
#include <math.h>
#include <iostream>
using namespace std;

void getSquare() {
    float a, b, c, d, root1, root2;
    cout << "Please enter a: ";
    cin >> a;
    cout << "Please enter b: ";
    cin >> b;
    cout << "Please enter c: ";
    cin >> c;
    cout << "D = " << b << "^2 + 4 * " << a << " * " << c <<endl;
    d=(b*b)+4*a*c;
    cout << "D = " << d << endl;
    if (d<0) {
        root1=(-b)/(2*a);
        root2=sqrt(-d)/(2*a);
        cout<<"Roots are imaginary";
    }
    else if (d==0) {
        root1=(-b)/(2*a);
        root2=root1;
        cout<<"Roots are real & equal";
    }
    else {
        root1=-(b+sqrt(d))/(2*a);
        root2=-(b-sqrt(d))/(2*a);
        cout<<"Roots are real & distinct" <<endl;
    }
    
    cout << "root1 = " << root1 << endl;
    cout << "root2 = " << root2 << endl;
}
